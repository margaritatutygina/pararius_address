import requests
import os
import re
import peewee
from peewee import fn
import pandas as pd
import datetime
from database.tortugacapital import Pararius
from settingsstore import SettingsStore


settingsstore = SettingsStore(store='/tortuga')

db_host = 'tortuganewdb.c2364vneslhl.eu-central-1.rds.amazonaws.com'
db_port = 5432
db_name = 'tortugacapital'
db_user = settingsstore.get('db_user')
db_password = settingsstore.get('db_password')

nr_addresses = (Pararius.select()
                .count())

print(nr_addresses)

# list_address = (Pararius.select(Pararius.straat).order_by(Pararius.straat).dicts())

# for i in list_address:
#     print(i)

apt_name = (Pararius.select().order_by(Pararius.straat))

# for i in apt_name:
#     # print(i.apt_name)
#     parts = re.split('(\D+)', i.apt_name)
#     # print(parts[2])
#     # print(type(i))
#     print("apt: ", i.apt_name)

#     if len(parts) > 3 and len(parts[3]) > 4:
#         if len (parts) > 5:
#             i.huisnummer = parts[4]
#             i.huisletter = parts[5].replace('-', '')
#             i.addition = parts[6]

#             # print("huisnummer: ", str(i.huisnummer))
#             # print("huisletter: ", i.huisletter)
#             # print("Addition: ", i.addition)

#         else:
#             i.huisnummer = parts[4]
#             i.huisletter = ""
#             i.addition = ""

#             # print("huisnummer: ", str(i.huisnummer))
#             # print("huisletter: ", i.huisletter)
#             # print("Addition: ", i.addition)

#     elif len(parts) > 3:
#         i.huisnummer = parts[2]
#         i.huisletter = parts[3].replace('-', '')
#         i.addition = parts[4]

#         # print("huisnummer: ", str(i.huisnummer))
#         # print("huisletter: ", i.huisletter)
#         # print("Addition: ", i.addition)

#     else:
#         i.huisnummer = parts[2]
#         i.huisletter = ""
#         i.addition = ""

#         # print("huisnummer: ", str(i.huisnummer))
#         # print("huisletter: ", i.huisletter)
#         # print("Addition: ", i.addition)

#     i.full_address = i.straat.lower().replace(" ","") + str(i.huisnummer) + "-" + i.huisletter.lower().strip() + i.addition.lower().strip()
#     if i.huisnummer == "":
#         i.huisnummer = None 
#     (Pararius.update(full_address = i.full_address, huisnummer = i.huisnummer, huisletter = i.huisletter, huisnummertoevoeging = i.huisnummertoevoeging).where(Pararius.web_prop_id == i.web_prop_id).execute())
#     print("updated: " + i.full_address)


# for i in apt_name:
#     i.postcode = i.postcode.replace(' ', '')
#     (Pararius.update(postcode = i.postcode ).where(Pararius.web_prop_id == i.web_prop_id).execute())
#     print('Updated postcode: ')