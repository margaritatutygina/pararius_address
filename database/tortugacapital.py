from peewee import *
import os
from settingsstore import SettingsStore

settingsstore = SettingsStore(store='/tortuga')

db_host = 'tortuganewdb.c2364vneslhl.eu-central-1.rds.amazonaws.com'
db_port = 5432
db_name = 'tortugacapital'
db_user = settingsstore.get('db_user')
db_password = settingsstore.get('db_password')

class DataBase(PostgresqlDatabase):
    def __init__(self, host=None, port=None, database=None, user=None, password=None, autorollback=True, **kwargs):
        kwargs.update({"user": user, "password": password, "autorollback": autorollback})
        kwargs.update({"host": host, "port": port})
        super().__init__(database, **kwargs)

database = DataBase(host=db_host, port=db_port, database=db_name, user=db_user, password=db_password)


class UnknownField(object):
    def __init__(self, *_, **__):
        pass

class BaseModel(Model):
    class Meta:
        database = database


class AddressRelations(BaseModel):
    address = TextField(null=True)
    kadaster_id = BigIntegerField(null=True)
    related_id = TextField(null=True)
    related_names = TextField(null=True)

    class Meta:
        table_name = 'Address_relations'
        schema = 'source'
        primary_key = False

class Bag(BaseModel):
    gemeente = TextField(null=True)
    huisletter = TextField(null=True)
    huisnummer = IntegerField(null=True)
    huisnummertoevoeging = TextField(null=True)
    lat = FloatField(null=True)
    lon = FloatField(null=True)
    nevenadres = TextField(null=True)
    nummeraanduiding = TextField(null=True)
    object_id = BigIntegerField(index=True, null=True)
    object_type = TextField(null=True)
    openbareruimte = TextField(null=True)
    oppervlakteverblijfsobject = IntegerField(null=True)
    pandbouwjaar = IntegerField(null=True)
    pandid = BigIntegerField(index=True, null=True)
    pandstatus = TextField(null=True)
    postcode = TextField(null=True)
    provincie = TextField(null=True)
    straathuisnummer = TextField(null=True)
    temp_key = TextField(index=True, null=True)
    verblijfsobjectgebruiksdoel = TextField(null=True)
    verblijfsobjectstatus = TextField(null=True)
    woonplaats = TextField(null=True)

    class Meta:
        table_name = 'BAG'
        schema = 'source'
        primary_key = False

class BagRotterdamSchiedam(BaseModel):
    gemeente = TextField(null=True)
    huisletter = TextField(null=True)
    huisnummer = IntegerField(null=True)
    huisnummertoevoeging = TextField(null=True)
    lat = FloatField(null=True)
    lon = FloatField(null=True)
    nevenadres = TextField(null=True)
    nummeraanduiding = TextField(null=True)
    object_id = BigIntegerField(index=True, null=True)
    object_type = TextField(null=True)
    openbareruimte = TextField(null=True)
    oppervlakteverblijfsobject = IntegerField(null=True)
    pandbouwjaar = IntegerField(null=True)
    pandid = BigIntegerField(index=True, null=True)
    pandstatus = TextField(null=True)
    postcode = TextField(null=True)
    provincie = TextField(null=True)
    temp_key = TextField(index=True, null=True)
    verblijfsobjectgebruiksdoel = TextField(null=True)
    verblijfsobjectstatus = TextField(null=True)
    woonplaats = TextField(null=True)

    class Meta:
        table_name = 'BAG_Rotterdam_Schiedam'
        schema = 'source'
        primary_key = False

class Funda(BaseModel):
    aangeboden_sinds = TextField(column_name='Aangeboden sinds', null=True)
    aantal_kamers = TextField(column_name='Aantal kamers', null=True)
    aantal_woonlagen = TextField(column_name='Aantal woonlagen', null=True)
    achtertuin = TextField(column_name='Achtertuin', null=True)
    bouwjaar = DateField(column_name='Bouwjaar', null=True)
    city = TextField(column_name='City', null=True)
    eigendomssituatie = TextField(column_name='Eigendomssituatie', null=True)
    energielabel = TextField(column_name='Energielabel', null=True)
    gelegen_op = TextField(column_name='Gelegen op', null=True)
    inhoud = BigIntegerField(column_name='Inhoud', null=True)
    jaarlijkse_vergadering = TextField(column_name='Jaarlijkse vergadering', null=True)
    onderhoudsplan = TextField(column_name='Onderhoudsplan', null=True)
    oorspronkelijke_vraagprice = BigIntegerField(column_name='Oorspronkelijke_vraagprice', null=True)
    opstalverzekering = TextField(column_name='Opstalverzekering', null=True)
    perceel = BigIntegerField(column_name='Perceel', null=True)
    postal_code = TextField(column_name='Postal Code', null=True)
    price = BigIntegerField(column_name='Price', null=True)
    reservefonds_aanwezig = TextField(column_name='Reservefonds aanwezig', null=True)
    schuur_berging = TextField(column_name='Schuur/berging', null=True)
    soort_appartement = TextField(column_name='Soort appartement', null=True)
    soort_bouw = TextField(column_name='Soort bouw', null=True)
    soort_parkeergelegenheid = TextField(column_name='Soort parkeergelegenheid', null=True)
    soort_woonhuis = TextField(column_name='Soort woonhuis', null=True)
    specifiek = TextField(column_name='Specifiek', null=True)
    status = TextField(column_name='Status', null=True)
    street = TextField(column_name='Street', null=True)
    tuin = TextField(column_name='Tuin', null=True)
    verwarming = TextField(column_name='Verwarming', null=True)
    voorlopig_energielabel = TextField(column_name='Voorlopig energielabel', null=True)
    vraagprice = BigIntegerField(column_name='Vraagprice', null=True)
    warm_water = TextField(column_name='Warm water', null=True)
    wonen = BigIntegerField(column_name='Wonen', null=True)
    date = DateField(null=True)
    script_version = IntegerField(null=True)

    class Meta:
        table_name = 'Funda'
        schema = 'source'
        primary_key = False

class FundaObjects(BaseModel):
    aangeboden_sinds = TextField(column_name='Aangeboden sinds', null=True)
    aantal_kamers = TextField(column_name='Aantal kamers', null=True)
    aantal_woonlagen = TextField(column_name='Aantal woonlagen', null=True)
    achtertuin = TextField(column_name='Achtertuin', null=True)
    bouwjaar = TextField(column_name='Bouwjaar', null=True)
    city = TextField(column_name='City', null=True)
    eigendomssituatie = TextField(column_name='Eigendomssituatie', null=True)
    energielabel = TextField(column_name='Energielabel', null=True)
    gelegen_op = TextField(column_name='Gelegen op', null=True)
    inhoud = TextField(column_name='Inhoud', null=True)
    jaarlijkse_vergadering = TextField(column_name='Jaarlijkse vergadering', null=True)
    onderhoudsplan = TextField(column_name='Onderhoudsplan', null=True)
    oorspronkelijke_vraagprijs = TextField(column_name='Oorspronkelijke vraagprijs', null=True)
    opstalverzekering = TextField(column_name='Opstalverzekering', null=True)
    perceel = TextField(column_name='Perceel', null=True)
    postal_code = TextField(column_name='Postal Code', null=True)
    price = TextField(column_name='Price', null=True)
    reservefonds_aanwezig = TextField(column_name='Reservefonds aanwezig', null=True)
    schuur_berging = TextField(column_name='Schuur/berging', null=True)
    soort_appartement = TextField(column_name='Soort appartement', null=True)
    soort_bouw = TextField(column_name='Soort bouw', null=True)
    soort_parkeergelegenheid = TextField(column_name='Soort parkeergelegenheid', null=True)
    soort_woonhuis = TextField(column_name='Soort woonhuis', null=True)
    specifiek = TextField(column_name='Specifiek', null=True)
    status = TextField(column_name='Status', null=True)
    street = TextField(column_name='Street', null=True)
    tuin = TextField(column_name='Tuin', null=True)
    verwarming = TextField(column_name='Verwarming', null=True)
    voorlopig_energielabel = TextField(column_name='Voorlopig energielabel', null=True)
    vraagprijs = TextField(column_name='Vraagprijs', null=True)
    warm_water = TextField(column_name='Warm water', null=True)
    wonen = TextField(column_name='Wonen', null=True)
    date = DateField(null=True)
    script_version = IntegerField(null=True)

    class Meta:
        table_name = 'Funda_objects'
        schema = 'source'
        primary_key = False

class Gmaps(BaseModel):
    company_address = TextField(null=True)
    date = DateField(null=True)
    kadaster_id = BigIntegerField(index=True, null=True)
    owner_row_owner = TextField(null=True)
    phone_number = TextField(null=True)
    script_version = IntegerField(null=True)
    website = TextField(null=True)

    class Meta:
        table_name = 'Gmaps'
        schema = 'source'
        primary_key = False

class Kadaster(BaseModel):
    bag_id = BigIntegerField(column_name='BAG_id', index=True, null=True)
    bag_pandid = BigIntegerField(column_name='BAG_pandid', null=True)
    date = DateField(null=True)
    house_numer_extension = TextField(null=True)
    indicatie_overleden = TextField(null=True)
    natuurlijke_persoon_code = TextField(null=True)
    object_extracted_house_letter = TextField(null=True)
    object_extracted_house_number = BigIntegerField(null=True)
    object_extracted_street_name = TextField(null=True)
    object_kadaster_name = TextField(null=True)
    object_raw_street_plus_number = TextField(null=True)
    object_raw_zipcode_plus_city = TextField(null=True)
    objectnummer = TextField(null=True)
    owner_kvk_nummer = BigIntegerField(null=True)
    owner_naam = TextField(null=True)
    owner_persoonsnummer = BigIntegerField(index=True, null=True)
    owner_row_owner = TextField(index=True, null=True)
    owner_subjectnummer = TextField(null=True)
    owner_voorletters = TextField(null=True)
    owner_voornaam = TextField(null=True)
    owner_voorvoegsel = TextField(null=True)
    zipcode = TextField(column_name='postcode', null=True)
    source = TextField(null=True)
    telling = IntegerField(null=True)
    temp_key = TextField(index=True, null=True)
    woonplaats = TextField(null=True)

    class Meta:
        table_name = 'Kadaster'
        schema = 'source'
        primary_key = False

class KadasterOwnerInfo(BaseModel):
    birth_date = TextField(null=True)
    birth_place = TextField(null=True)
    date = DateField(null=True)
    number_betrokkene = IntegerField(null=True)
    number_rechthebbende = IntegerField(null=True)
    owner_full_name = TextField(null=True)
    owner_kvk = BigIntegerField(null=True)
    owner_persoonsnummer = BigIntegerField(index=True, null=True)
    owner_street_plus_number = TextField(null=True)
    owner_zipcode_plus_city = TextField(null=True)
    related_companies = TextField(null=True)
    script_version = IntegerField(null=True)
    source = TextField(null=True)

    class Meta:
        table_name = 'Kadaster_owner_info'
        schema = 'source'
        primary_key = False

class Kvk(BaseModel):
    city = TextField(null=True)
    company = TextField(null=True)
    date = DateField(null=True)
    kadaster_id = BigIntegerField(index=True, null=True)
    kvks = TextField(index=True, null=True)
    script_version = IntegerField(null=True)
    street_name = TextField(null=True)
    zip_code = TextField(null=True)

    class Meta:
        table_name = 'Kvk'
        schema = 'source'
        primary_key = False

class Leadr(BaseModel):
    bikcode = TextField(null=True)
    company_address = TextField(null=True)
    company_size = TextField(null=True)
    company_start_date = DateField(null=True)
    date = DateField(null=True)
    email = TextField(null=True)
    kadaster_id = BigIntegerField(index=True, null=True)
    kvk_entry_type = TextField(null=True)
    owner_row_owner = TextField(null=True)
    phone_number = TextField(null=True)
    script_version = IntegerField(null=True)
    source = TextField(null=True)
    website = TextField(null=True)

    class Meta:
        table_name = 'Leadr'
        schema = 'source'
        primary_key = False

class Pipedrive(BaseModel):
    date = DateField(null=True)
    kadaster_id = BigIntegerField(index=True, null=True)
    owner_row_owner = TextField(null=True)

    class Meta:
        table_name = 'Pipedrive'
        schema = 'source'
        primary_key = False

class PipedriveTemp(BaseModel):
    date = DateField(null=True)
    kadaster_id = BigIntegerField(index=True, null=True)
    name = TextField(null=True)

    class Meta:
        table_name = 'Pipedrive_temp'
        schema = 'source'
        primary_key = False

class PortfolioRelations(BaseModel):
    bag_id = BigIntegerField(column_name='BAG_id', index=True, null=True)
    related_kadaster_ids = TextField(null=True)
    original_kadaster_id = TextField(null=True)
    related_names = TextField(null=True)

    class Meta:
        table_name = 'Portfolio_relations'
        schema = 'source'
        primary_key = False

class PortfolioRelationsOwners(BaseModel):
    related_ids = TextField(null=True)
    kadaster_id = BigIntegerField(null=True)
    related_names = TextField(null=True)

    class Meta:
        table_name = 'Portfolio_relations_owners'
        schema = 'source'
        primary_key = False

class realworks(BaseModel):
    asking_price = BigIntegerField(null=True)
    asking_price_m2 = BigIntegerField(null=True)
    broker = TextField(null= True)
    building_year = TextField(null = True)
    content = BigIntegerField(null= True)
    date_announced = DateField(null= True)
    date_transaction = DateField(null= True)
    days_on_market = IntegerField(null= True)
    end_date_energy_label = DateField(null=True)
    energy_index = FloatField(null=True)
    energy_label = TextField(null = True)
    full_address = TextField(column_name='full_address', null=True)
    house_letter = TextField(null= True)
    house_number = TextField(null= True)
    lot_size = BigIntegerField(null= True)
    m2 = IntegerField(null= True)
    nr_bedrooms = IntegerField(null=True)
    nr_rooms = IntegerField(null=True)
    postcode = TextField(null= True)
    quality = TextField(null = True)
    sort_appartment = TextField(null = True)
    sort_house = TextField(null = True)
    sort_real_estate = TextField(null = True)
    street = TextField(null= False)
    transaction_price = BigIntegerField(null=True)
    transaction_price_m2 = BigIntegerField(null=True)
    transaction_type = TextField(null= True)
    type_house = TextField(null = True)
    woonplats = TextField(null= True)


    class Meta:
        table_name = 'realworks'
        schema = 'source'
        primary_key = False

class objects_realworks(BaseModel):
    street = TextField(null= False)
    house_number = TextField(null= True)
    house_letter = TextField(null= True)
    postcode = TextField(null= True)
    woonplats = TextField(null= True)
    broker = TextField(null= True)
    transaction_type = TextField(null= True)
    asking_price = BigIntegerField(null=True)
    asking_price_m2 = BigIntegerField(null=True)
    date_announced = DateField(null= True)
    transaction_price = BigIntegerField(null=True)
    transaction_price_m2 = BigIntegerField(null=True)
    date_transaction = DateField(null= True)
    days_on_market = IntegerField(null= True)
    quality = TextField(null = True)
    energy_label = TextField(null = True)
    energy_index = FloatField(null=True)
    end_date_energy_label = DateField(null=True)
    building_year = TextField(null = True)
    nr_rooms = IntegerField(null=True)
    nr_bedrooms = IntegerField(null=True)
    full_address = TextField(column_name='full_address', null=True)
    straathuisnummer = TextField(null=True)
    object_id = BigIntegerField(null=True)
    pandid = BigIntegerField(null= True)
    lat = DecimalField(null=True)
    lon = DecimalField(null=True)
    woonplats = TextField(null= True)
    Buurt = TextField(null= True)

    class Meta:
        table_name = 'objects_realworks'
        schema = 'business'
        primary_key = False

class Telefoonboek(BaseModel):
    date = DateField(null=True)
    kadaster_id = BigIntegerField(index=True, null=True)
    owner_address = TextField(null=True)
    owner_row_owner = TextField(null=True)
    phone_number = TextField(null=True)
    script_version = IntegerField(null=True)
    source = TextField(null=True)

    class Meta:
        table_name = 'Telefoonboek'
        schema = 'source'
        primary_key = False

class RentalPricesHistorical(BaseModel):
    address = TextField(null=True)
    huisnummer = IntegerField(null=True)
    huisletter = TextField(null=True)
    huisnummertoevoeging = TextField(null=True)
    net_rent_month = DecimalField(null=True)
    date = DateField(null=False)
    source = TextField(null=True)
    object_id = BigIntegerField(null=True)
    street = TextField(null=True)
    full_address = TextField(null=True)
    idx = IntegerField(null=True)
    data_owner = TextField(null=True)

    class Meta:
        table_name = 'rental_prices_historical'
        schema = 'source'
        primary_key = False

class Pararius(BaseModel):
    web_prop_id = TextField(null=True)
    apt_name = TextField(null=True)
    straat = TextField(null=True)
    buurt = TextField(null=True)
    stad = TextField(null=True)
    postcode = TextField(null=True)
    oppervlakte = IntegerField(null=True)
    huurprijs = IntegerField(null=True)
    slaapkamers = IntegerField(null=True)
    woningtype = TextField(null=True)
    utilities = TextField(null=True)
    gemeubileerd = TextField(null=True)
    aangeboden_sinds = DateField(null=True)
    aangeboden_descr = TextField(null=True)
    bouwjaar = IntegerField(null=True)
    makelaar = TextField(null=True)
    latitude = DecimalField(null=True)
    longitude = DecimalField(null=True)
    script_date = DateField(null=True)
    script_version = IntegerField(null=True)
    source = TextField(null=True)
    full_address = TextField(null=True)
    huisnummer = IntegerField(null=True)
    huisletter = TextField(null=True)
    huisnummertoevoeging = TextField(null=True)


    class Meta:
        table_name = 'Pararius'
        schema = 'source'
        primary_key = False
