import os

import boto3
from botocore.exceptions import ClientError


class SettingsStore:
    store = None

    def __init__(self, aws_profile_name=None, store=None):
        self.store = store
        self.aws_profile_name = aws_profile_name

        session = boto3.Session(profile_name=self.aws_profile_name)
        self.ssm_client = session.client('ssm')

    def _get_from_parameter_store(self, abspath):
        try:
            parameter = self.ssm_client.get_parameter(
                Name=abspath,
                WithDecryption=True
            )
        except self.ssm_client.exceptions.ParameterNotFound:
            msg = f"Parameter '{abspath}' not found in AWS SSM Parameter Store for AWS profile: {self.aws_profile_name}"
            print(msg)
            raise (Exception(msg))
        except ClientError as e:
            msg = f"Error code while retrieving parameter from store: {e.response['Error']['Code']}"
            print(msg)
            raise (Exception(msg))

        try:
            value = parameter['Parameter']['Value']
        except KeyError:
            msg = f"Error while retrieving parameter from store. Can't find value in the response: {parameter}"
            print(msg)
            raise (Exception(msg))

        return value

    def _get_abspath(self, store, item):
        store = (store or "").rstrip("/").lstrip("/")
        abspath = f'/{store}/{item}'.replace('//', '/')

        return abspath

    def _get_from_environment(self, abspath):
        envname = abspath.replace('/', '_').upper()
        value = os.environ.get(envname)
        return value

    def _get(self, store, item):
        abspath = self._get_abspath(store, item)

        # First try if there is a local environment variable override
        value = self._get_from_environment(abspath)

        if not value:
            # Not found in environment, get from parameter store
            value = self._get_from_parameter_store(abspath)

        return value

    def get(self, item):
        """
        Get the value for a configuration item in the store.
        :param item: The name of the configuration item you want from the Parameter Store.
        :return: The value of the configuration item in the Parameter Store, possibly overrided by a
        local environment variable (uppercase and change slashed for underscores)
        """
        return self._get(self.store, item)